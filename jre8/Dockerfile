FROM openjdk:8-alpine

ENV ROOTDIR /usr/local
ENV LD_LIBRARY_PATH /usr/local/lib
ENV GDAL_VERSION 2.4.0
ENV OPENJPEG_VERSION 2.3.0

# Load assets
WORKDIR $ROOTDIR/
RUN mkdir -p $ROOTDIR/src

RUN wget -qO- \
    http://download.osgeo.org/gdal/${GDAL_VERSION}/gdal-${GDAL_VERSION}.tar.gz | \
    tar -xzC $ROOTDIR/src/
RUN wget -qO- \
    https://github.com/uclouvain/openjpeg/archive/v${OPENJPEG_VERSION}.tar.gz | \
    tar -xzC $ROOTDIR/src/

RUN set -ex \
    # Install runtime dependencies
    && apk add --update --no-cache \
    git \
    bash-completion \
    python-dev \
    python3-dev \
    libffi-dev \
    jpeg-dev \
    openjpeg-dev \
    libpng-dev \
    linux-headers \
    && apk add --no-cache \
    --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing \
    proj4 \
    # Install common build dependencies
    && apk add --no-cache --virtual .build-deps \
    gcc \
    cmake \
    build-base \
    make \
    swig \
    apache-ant \
    # Compile and install OpenJPEG
    && cd src/openjpeg-${OPENJPEG_VERSION} \
    && mkdir build && cd build \
    && cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$ROOTDIR \
    && make -j3 && make -j3 install && make -j3 clean \
    && cd $ROOTDIR && rm -Rf src/openjpeg* \
    # Compile and install GDAL
    && cd src/gdal-${GDAL_VERSION} \
    && ./configure --with-python --with-curl --with-java=${JAVA_HOME} --with-openjpeg=$ROOTDIR \
    && make -j3 && make -j3 install \
    # Compile Python and Java bindings for GDAL
    && cd $ROOTDIR/src/gdal-${GDAL_VERSION}/swig/java && make -j3 && make -j3 install \
    && cd $ROOTDIR/src/gdal-${GDAL_VERSION}/swig/python \
    && python3 setup.py build \
    && python3 setup.py install \
    && cd $ROOTDIR/src/gdal-${GDAL_VERSION}/swig/java && cp -f ./.libs/*.so* $LD_LIBRARY_PATH \
    && cp -f gdal.jar $LD_LIBRARY_PATH \
    && cd $ROOTDIR && rm -Rf src/gdal* \
    # Remove build dependencies
    && apk del .build-deps \
