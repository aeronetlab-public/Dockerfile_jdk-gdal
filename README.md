# Java-Geo Docker Image

Provides Dockerfile to build image with Java and various Geo tools.

To use as base image for Java-based Geo Applications since some tools (like GDAL) require a great deal of time to build.

Based on `openjdk:8-alpine`

## List of included tools:

- GDAL
- Java bindings
- Python bindings
